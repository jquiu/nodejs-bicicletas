var BikeSchema = function (id, color, model, location) {
    this.id = id
    this.color = color
    this.model = model
    this.location = location
}

BikeSchema.prototype.toString = function () {
    return `id: ${this.id} | color: ${this.color}`
}

BikeSchema.allBikes = []
BikeSchema.add = function (aBike) {
    BikeSchema.allBikes.push(aBike)
}

BikeSchema.findIndexById = function (aBiciId) {
    const condicion = (e) => e.id == aBiciId;
    const index = BikeSchema.allBikes.findIndex(condicion)
    return index < 0 ? null : index
}

BikeSchema.findById = function (aBiciId) {
    const index = BikeSchema.findIndexById(aBiciId)
    return index == null ? null : BikeSchema.allBikes[index]
}

BikeSchema.removeById = function (aBiciId) {
    const index = BikeSchema.findIndexById(aBiciId)
    if (index != null) { BikeSchema.allBikes.splice(index, 1) }
}

const newBikeA = new BikeSchema(1, 'red', 'Mountain bike', [6.2014825, -75.5991463]);
const newBikeB = new BikeSchema(2, 'blue', 'Racing bicycle', [6.212500, -75.600262]);
const newBikeC = new BikeSchema(3, 'green', 'Tourism bicycle', [6.222046, -75.596485]);
const newBikeD = new BikeSchema(4, 'black', 'Folding bicycle', [6.206682, -75.598598]);
const newBikeE = new BikeSchema(5, 'pink', 'Hybrid bicycle', [6.204619, -75.585819]);

BikeSchema.add(newBikeA);
BikeSchema.add(newBikeB);
BikeSchema.add(newBikeC);
BikeSchema.add(newBikeD);
BikeSchema.add(newBikeE);

module.exports = BikeSchema
