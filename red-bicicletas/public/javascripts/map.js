var map = L.map('mapid', {
    center: [6.248885, -75.519581],
    zoom: 13
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)


$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function (result) {
        result.bikes.forEach(function (bike) {
            L.marker(bike.location, { title: bike.id }).addTo(map)
        })
    }
})