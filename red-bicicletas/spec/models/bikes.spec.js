var Bike = require('../../models/bike');
const BikeSchema = require('../../models/bike');

beforeEach(() => { Bike.allBikes = [] });

describe("Bike.allBikes", () => {
  it("empty start", function () {
    expect(Bike.allBikes.length).toBe(0);
  })
});

describe('Bike.add', () => {
  it("add one bike", () => {
    expect(Bike.allBikes.length).toBe(0);
    var a = new BikeSchema(1, 'red', 'urbans', [6.6718, -75.0638]);
    Bike.add(a);

    expect(Bike.allBikes.length).toBe(1);
    expect(Bike.allBikes[0]).toBe(a);
  })
})
