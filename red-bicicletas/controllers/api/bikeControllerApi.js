const BikeSchema = require('../../models/bike');

exports.bikeList = (req, res) => {
    res.status(200).json({
        bikes: BikeSchema.allBikes
    });
};


exports.bikeCreate = (req, res) => {
    Bicicleta.removeById(req.body.id)
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
    bici.ubicacion = [req.body.lat, req.body.lng]

    Bicicleta.add(bici)

    res.status(200).json({ bicicleta: bici })
}

exports.bikeUpdate = (req, res) => {
    var bici = Bicicleta.findById(req.body.id_busqueda)
    if (bici) {
        bici.id = req.body.id
        bici.color = req.body.color
        bici.modelo = req.body.modelo
        bici.ubicacion = [req.body.lat, req.body.lng]
        res.status(200).json({ bicicleta: bici })
    } else {
        res.send("no se encontró una bici con id " + req.body.id_busqueda)
    }
}

exports.bikeDelete = (req, res) => {
    Bicicleta.removeById(req.body.id)
    res.status(204).send()
}