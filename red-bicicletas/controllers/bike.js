const BikeSchema = require('../models/bike');

exports.bikeList = (req, res) => {
    res.render('bikes/index', {
        bikes: BikeSchema.allBikes
    });
};

exports.createBikeGet = (req, res) => {
    res.render('bikes/create');
};

exports.createBikePost = (req, res) => {
    var bikeLocation = [req.body.lat, req.body.lng];
    var bike = new BikeSchema(req.body.id, req.body.color, req.body.model, bikeLocation);
    BikeSchema.add(bike);
    res.render('bikes/index', {
        bikes: BikeSchema.allBikes
    });
};

exports.updateBikeGet = function (req, res) {
    var bike = BikeSchema.findById(req.params.id)
    res.render('bikes/update', { bike })
}

exports.updateBikePost = function (req, res) {
    var bike = BikeSchema.findById(req.params.id)
    bike.id = req.body.id
    bike.color = req.body.color
    bike.model = req.body.model
    bike.location = [req.body.lat, req.body.lng]

    res.redirect('/bikes')
}

exports.deleteBike = function (req, res) {
    BikeSchema.removeById(req.body.id)

    res.redirect('/bikes')
}