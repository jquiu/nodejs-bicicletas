var express = require('express');
var router = express.Router();
var controllerBike = require('../controllers/bike');

/* bike list pages */
router.get('/', controllerBike.bikeList);
router.get('/create', controllerBike.createBikeGet);
router.post('/create', controllerBike.createBikePost);
router.get('/:id/update', controllerBike.updateBikeGet);
router.post('/:id/update', controllerBike.updateBikePost);
router.post('/:id/delete', controllerBike.deleteBike);

module.exports = router;