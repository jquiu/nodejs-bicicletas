var express = require('express');
var router = express.Router();
var apiBike = require('../../controllers/api/bikeControllerApi');

/* bike list pages */
router.get('/', apiBike.bikeList);
router.post('/create', apiBike.bikeCreate);
router.post('/:id/update', apiBike.bikeUpdate);
router.post('/:id/delete', apiBike.bikeDelete);

module.exports = router;