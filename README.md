# README #

Proyecto de nodeJS en el que cual se visualizan bicicletas sobre un mapa. se puede realizar un CRUD  de bicicletas desde una interfaz gráfica.

### How do I get set up? ###

* Intalar nodeJS nodejs.org
* Clonar (descargar) el proyecto Red_bicicletas en tu computador
* Entrar a la ubicación del proyecto desde la consola y ejecutar el comando npm install
* Ejecutar el comando npm run devstart.
* ir al navegador y abrir http://localhost:3000/
